# LPIC3- 303 Security  @AlpacaKvg @isx25633105 ASIX [Trello](https://trello.com/b/5uoQ0YfZ/lpic-3-303-security) Curs 2021-2022
![cartel](cartel/cartel.png)
# ÍNDICE
- **[Introducción](#introduccion)**
- **[Defensa](#defensa)**
    - **[DNSSEC](https://gitlab.com/LPIC-3-303-Security/asix-2022/-/tree/master/DNSSEC)**
- **[Ataques](#ataques)**
    - **[XSS-BeEF](https://gitlab.com/LPIC-3-303-Security/asix-2022/-/tree/master/XSS-BeEF)**
    - **[log4shell](https://gitlab.com/LPIC-3-303-Security/asix-2022/-/tree/master/log4shell)**
    - **[pdf_infected](https://gitlab.com/LPIC-3-303-Security/asix-2022/-/tree/master/pdf_infected)**
    - **[phising](https://gitlab.com/LPIC-3-303-Security/asix-2022/-/tree/master/phishing)**
    - **[reverse_shell](https://gitlab.com/LPIC-3-303-Security/asix-2022/-/tree/master/reverse_shell)**
    - **[spoofing](https://gitlab.com/LPIC-3-303-Security/asix-2022/-/tree/master/spoofing)**
- **[Video presentación](#video)**
- **[Bibliografía](#biblio)**
# INTRODUCCIÓN<a name="introduccion"></a>
Uno de los pilares más importantes de la informática es la ciberseguridad. Es por eso que este trabajo se basa en explicar y mostrar el funcionamiento de diversos ataques y como mitigarlos.

## **Defensa:**<a name="defensa"></a>
### **DNSSEC:**
El sistema DNS fué diseñado hace muchos años, cuando el internet aún no era un lugar tan poblado ni tan peligroso como actualmente.  
Este protocolo ya dispone de una pequeña protección contra intrusos maliciosos, pero no es suficiente a día de hoy y es altamente vulnerable a ataques como el spoofing.
Por esto mismo, se creo el DNSSEC (Domain Name System Security Extensions) unas extensiones de seguridad que añaden las firmas digitales en los datos del DNS para así poder validar la integridad y autenticidad de esta información.
## **Ataques:**<a name="ataques"></a>
### **XSS-BeEF:**
El xss o cross-site scripting es una vulnerabilidad encontrada frecuentemente en webs i aplicaciones que aceptan input por parte de los usuarios, ya sean cajas de comentarios, formularios o mensajes.

En resumen, este ataque utiliza la inyección de código en paginas web para ejecutar scripts o comandos y así poder ganar acceso o poder obtener datos de los clientes. Es peligroso ya que bien ejecutado pasa desapercibido completamente y el usuario puede estar siendo victima sin darse cuenta, ya que los scripts se ejecutan en background.

Actualmente es una de las vulnerabilidades más frecuentes en el entorno web y es muy complejo poder asegurarse del todo contra esta misma ya que dispone de multiples variantes y cualquier mínimo fallo puede resultar en una vulnerabilidad grande.

### **Log4shell:**
Log4j es una librería de código abierto creada por apache. Miles de servidores/programas integran esta herramienta para monitorizar su actividad. La información obtenida en los registros que genera log4shell puede emplearse para encontrar errores en el servidores/programas que estás analizando. Esta vulnerabilidad podía permitir a un atacante ejecutar código en un equipo ajeno de forma remota. 

El principal problema y que convierte esta vulnerabilidad en una de las peores de los últimos tiempos es debido a que la librería log4j és muy usada. Esta vulnerabilidad afecta desde tu propio router de casa hasta la aplicación para acceder el correo electrónico.

### **Pdf_infectado:**
Un pdf infectado, es un pdf modificado aprovechando vulnerabilidades de el propio programa adobe pdf. El cual nos pude permitir desde ejecutar una orden en powershell, hasta poder controlar el dispositivo del otro usuario. Enviar un pdf infectado es una practica muy habitual de los atacantes hacia grandes empresas, debido a que el software esta desactualizado y són más propensos a que el ataque tenga cierto éxito.

### **Phishing:**
El phishing es unos de los ataques más comunes en el día de hoy, todo esto gracias a que todo el mundo dispone de correo electrónico, teléfono o formas de comunicarse con el resto. Usualmente no es un ataque que sea muy efectivo en gente con conocimientos básicos de informática, por eso su target son personas mayores o gente que no tenga mucho conocimiento sobre este sector.

En resumen, el phishing no es más que una explotación de la ingeniería social mediante la cual el atacante se hace pasar por un servicio o tercero haciendo uso de la confianza/ignorancia de algunas victimas para obtener datos sensibles de esta. Es uno de los ataques más sencillos y efectivos, esto gracias al desconocimiento e imprudencia de los usuarios casuales, quienes no tienen mucho conocimiento de informática o ciberseguridad, especialmente la gente mayor.

### **Reverse_shell:**
Reverse shell se basa en la creación de una shell remota usando como base la propia shell que se está ejecutando en estos momentos. Para ejecutar un reverse shell normalmente se utilizan dos maquinas un atacante que sería el que ejecuta la shell remota y la victima que sería el equipo cuya shell remota el atacante desea obtener.

### **Spoofing:** 
El spoofing es un método de ataque basado en la falsificación (de ahí proviene su nombre en inglés) para engañar a la victima y obtener algo. Su objetivo es desde  obtener tu ip, hasta tarjetas de crédito, credenciales de banco o cualquier tipo de información sensible. El mecanismo de obtención de datos es simple, el atacante hace una pagina web falsa de algún lugar que tengas que iniciar session con tus credenciales. El atacante envía a la victima a traves de un medio de información el link, la victima entra a traves del link y inicia session. En ese momento el atacante ya tiene las credenciales de la victima. Hay diversos tipos de spoofing.

## **Video presentación:** <a name="video"></a>
![video](video-presentación/Video.mp4)
# BIBLIOGRAFÍA<a name="biblio"></a>
[DNSSEC](https://wiki.debian.org/DNSSEC%20Howto%20for%20BIND%209.9+)
<br/>
[Guia DNNSEC](https://www.youtube.com/watch?v=7ksgTFxAg6U)
<br/>
[Guia DNSSEC bind9](https://bind9.readthedocs.io/en/v9_17_12/dnssec-guide.html)
<br/>
[Guia log4shell](https://www.hackingarticles.in/a-detailed-guide-on-log4j-penetration-testing/)
* **Proyecto completamente en castellano** 

