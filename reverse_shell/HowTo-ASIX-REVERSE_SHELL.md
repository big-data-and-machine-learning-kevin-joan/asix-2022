
![x](imagenes/reverse-shell.png "x")

## Reverse Shell
# Indice de contenido 
1. [Teoria](#teoria)         
	+ [Tipos de ataques reverse shell](#ataques)         
		* [Local (local)](#local)         
		* [Remote](#remote)         
		* [Download and execute](#download)       
		* [Staged](#staged)
		* [Egg-hunt](#egg)
		* [Omelette](#omelette)     
	+ [Medidas de protección](#protect) 
2. [Practica](#practica)
	+ [Imagen Docker utilizada](#docker)     
	+ [Ejemplo](#ejemplo) 
	      

## Teoria <a name="teoria"></a>
Reverse shell se basa en la creación de una shell remota usando como base la propia shell que se está ejecutando en estos momentos. Para ejecutar un reverse shell normalmente se utilizan dos maquinas un atacante que sería el que ejecuta la shell remota y la victima que sería el equipo cuya shell remota el atacante desea obtener.
<br/>
* Explicación de reverse shell extraída de [Wikipedia](https://en.wikipedia.org/wiki/Shellcode):
>In hacking, a shellcode is a small piece of code used as the payload in the exploitation of a software vulnerability. It is called "shellcode" because it typically starts a command shell from which the attacker can control the compromised machine, but any piece of code that performs a similar task can be called shellcode. Because the function of a payload is not limited to merely spawning a shell, some have suggested that the name shellcode is insufficient.However, attempts at replacing the term have not gained wide acceptance. Shellcode is commonly written in machine code.
>
>When creating shellcode, it is generally desirable to make it both small and executable, which allows it to be used in as wide a variety of situations as possible. Writing good shellcode can be as much an art as it is a science. In assembly code, the same function can be performed in a multitude of ways and there is some variety in the lengths of opcodes that can be used for this purpose; good shellcode writers can put these small opcodes to use to create more compact shellcode. Some have reached the smallest possible size while maintaining stability. 
>

>Bind shell - attacker's machine acts as a client and victim's machine acts as a server opening up a communication port on the victim and waiting for the client to connect to it and then issue commands that will be remotely (with respect to the attacker) executed on the victim's machine. This would be only possible if the victim's machine has a public IP and is accessible over the internet (disregarding all firewall etc. for the sake of brevity).
>
>Now what if the victim's machine is NATed and hence not directly reachable ? One possible solution - So what if the victim's machine is not reachable. My (attacker's) machine is reachable. So let me open a server at my end and let the victim connect to me. This is what a reverse shell is.
>
>Reverse Shell - attacker's machine (which has a public IP and is reachable over the internet) acts as a server. It opens a communication channel on a port and waits for incoming connections. Victim's machine acts as a client and initiates a connection to the attacker's listening server. This is exactly what is done by the following.
>

## Tipos de ataques <a name="ataques"></a>
* Ejemplos de reverse shell extraídos de [Wikipedia](https://en.wikipedia.org/wiki/Shellcode):
>Shellcode can either be local or remote, depending on whether it gives an attacker control over the machine it runs on (local) or over another machine through a network (remote).
>
>**Local** <a name="local"></a>
>
>Local shellcode is used by an attacker who has limited access to a machine but can exploit a vulnerability, for example a buffer overflow, in a higher-privileged process on that machine. If successfully executed, the shellcode will provide the attacker access to the machine with the same higher privileges as the targeted process.
>
>**Remote** <a name="remote"></a>
>
>Remote shellcode is used when an attacker wants to target a vulnerable process running on another machine on a local network, intranet, or a remote network. If successfully executed, the shellcode can provide the attacker access to the target machine across the network. Remote shellcodes normally use standard TCP/IP socket connections to allow the attacker access to the shell on the target machine. Such shellcode can be categorized based on how this connection is set up: if the shellcode establishes the connection, it is called a "reverse shell" or a connect-back shellcode because the shellcode connects back to the attacker's machine. On the other hand, if the attacker establishes the connection, the shellcode is called a bindshell because the shellcode binds to a certain port on the victim's machine. There's a peculiar shellcode named bindshell random port that skips the binding part and listens on a random port made available by the operating system. Because of that the bindshell random port became the smallest and stable bindshell shellcode for x86_64 available to this date. A third, much less common type, is socket-reuse shellcode. This type of shellcode is sometimes used when an exploit establishes a connection to the vulnerable process that is not closed before the shellcode is run. The shellcode can then re-use this connection to communicate with the attacker. Socket re-using shellcode is more elaborate, since the shellcode needs to find out which connection to re-use and the machine may have many connections open.
>
>A firewall can be used to detect outgoing connections made by connect-back shellcode as well as incoming connections made by bindshells. They can therefore offer some protection against an attacker, even if the system is vulnerable, by preventing the attacker from connecting to the shell created by the shellcode. This is one reason why socket re-using shellcode is sometimes used: it does not create new connections and therefore is harder to detect and block.
>
>**Download and execute** <a name="download"></a>
>
>Download and execute is a type of remote shellcode that downloads and executes some form of malware on the target system. This type of shellcode does not spawn a shell, but rather instructs the machine to download a certain executable file off the network, save it to disk and execute it. Nowadays, it is commonly used in drive-by download attacks, where a victim visits a malicious webpage that in turn attempts to run such a download and execute shellcode in order to install software on the victim's machine. A variation of this type of shellcode downloads and loads a library. Advantages of this technique are that the code can be smaller, that it does not require the shellcode to spawn a new process on the target system, and that the shellcode does not need code to clean up the targeted process as this can be done by the library loaded into the process.
>
>**Staged** <a name="staged"></a>
>
>When the amount of data that an attacker can inject into the target process is too limited to execute useful shellcode directly, it may be possible to execute it in stages. First, a small piece of shellcode (stage 1) is executed. This code then downloads a larger piece of shellcode (stage 2) into the process's memory and executes it.
>
>**Egg-hunt** <a name="egg"></a>
>
>This is another form of staged shellcode, which is used if an attacker can inject a larger shellcode into the process but cannot determine where in the process it will end up. Small egg-hunt shellcode is injected into the process at a predictable location and executed. This code then searches the process's address space for the larger shellcode (the egg) and executes it.
>
>**Omelette** <a name="omelette"></a>
>
>This type of shellcode is similar to egg-hunt shellcode, but looks for multiple small blocks of data (eggs) and recombines them into one larger block (the omelette) that is subsequently executed. This is used when an attacker can only inject a number of small blocks of data into the process.
>
## Medidas de protección: <a name="protect"></a>
- Tener los dispositivos o programas actualizados y no dejar agujeros de seguridad cuando montemos cualquier sistema de red.

## Practica: <a name="practica"></a>

## Imagen docker utilizada para el ejemplo: <a name="docker"></a>
>En este ejemplo utilizamos una imagen de docker que contiene un vsftp que tiene una vulnerabilidad con la cual podemos conectarnos como root.
>
```sh
docker run --rm --name ftp.bad -p 21:21 -d isx25633105/vsftp2.3.4
```

## Ejemplo: <a name="ejemplo"></a>
- Ejemplo de reverse shell con version ftp infectado 2.3.4:

	- Pongamos por ejemplo que tenemos dos sistemas Linux, uno está en 10.200.243.206 como atacante y el otro está en 172.17.0.2 como víctima con el servidor ftp version 2.3.4 (vamos a utilizar la version infectada la cual a traves de un exploit te permite hacer un reverse shell).

	- Vamos a mirar si tiene el puerto 21 abierto (porque el exploit solo funciona con el puerto 21): 

	![21](imagenes/mirarpuertos21.png "21")

	- Una vez visto que tiene el puerto 21 abierto, empezamos a configurar la herramienta de exploit (la herramienta que utilizamos es msfconsole):

	![op](imagenes/miraropciones.png "op")

	- Configuramos el Host que va ha atacar y el puerto lo dejamos por defecto (el 21). Una vez configurado las opciones empezamos el ataque.

	![con](imagenes/con.png "con")
	
	- Como podemos observar en la imagen este exploit nos inicia un shell en el puerto 6200, podemos hacer todo por que somos root.

